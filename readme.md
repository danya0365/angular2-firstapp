# Fresh Setup

==============

## Set up

`npm install -g angular-cli`

# Create angular 2 new project

`ng new first-project`

# Build project

`ng serve`

# Edit Initial project

```
* try to edit src/app/_.component.css
* try to edit src/app/_.component.html
* try to edit src/app/_.component.ts
```

then save and see result in web browser

# Add Simple Component

`ng g c property-binding --flat -it -is` `ng g c event-binding --flat -it -is`
