import { Component } from '@angular/core';

@Component({
  selector: 'my-component',
  template: '<div>Hello my name is {{name}}. <button (click)="sayMyName()">Say my name</button></div>'
})
export class MyComponent {
    name: string = ''
  constructor() {
    this.name = 'Danya'
  }
  sayMyName() {
    console.log('My name is', this.name)
  }
}
