import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-property-binding',
  template: `
    <p>
    Result: {{result}}
    </p>
    <p>
    customResult: {{customResult}}
    </p>
  `,
  styles: [],
  // ***1
  inputs: ['customResult'] // like @Input at ***2
})
export class PropertyBindingComponent {
    // ***2
    @Input()  // like @Component({ inputs: ['result'] })
    result: number = 0

    customResult: number = 0;
}
