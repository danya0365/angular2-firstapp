import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-databinding',
  templateUrl: './databinding.component.html',
  styleUrls: ['./databinding.component.css']
})
export class DatabindingComponent implements OnInit {

    stringInterpolation: String = 'This is string interpolcation'
    numberInterpolation: number = 10;
  constructor() { }

  ngOnInit() {
  }

  onClicked(value: string) {
      alert(value);
  }
}
