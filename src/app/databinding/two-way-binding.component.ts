import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way-binding',
  template: `
    <input type="text" [(ngModel)]="person.name" />
    <input type="text" [(ngModel)]="person.lastName" />
    <input type="text" [(ngModel)]="person.age" />
  `,
  styles: []
})
export class TwoWayBindingComponent {

    person = {
        name: 'Marosdee',
        lastName: 'Uma',
        age: 30
    }
}
