import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { FirstProjectRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MyComponent } from './my.component';
import { OtherComponent } from './other';
import { DatabindingComponent } from './databinding/databinding.component';
import { PropertyBindingComponent } from './databinding/property-binding.component';
import { EventBindingComponent } from './databinding/event-binding.component';
import { TwoWayBindingComponent } from './databinding/two-way-binding.component';
import { LifecycleComponent } from './lifecycle.component';
import { FormComponent } from './form/form.component';
import { FormDataDrivenComponent } from './form/form-data-driven/form-data-driven.component';
import { FormDataDrivenFormGroupComponent } from './form/form-data-driven/form-data-driven-form-group.component';
import { PipesComponent } from './pipes/pipes.component';

@NgModule({
  declarations: [
    AppComponent,
    MyComponent,
    OtherComponent,
    DatabindingComponent,
    PropertyBindingComponent,
    EventBindingComponent,
    TwoWayBindingComponent,
    LifecycleComponent,
    FormComponent,
    FormDataDrivenComponent,
    FormDataDrivenFormGroupComponent,
    PipesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    FirstProjectRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
