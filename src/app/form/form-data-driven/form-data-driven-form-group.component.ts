import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
    selector: 'app-form-data-driven-form-group',
    templateUrl: './form-data-driven-form-group.component.html',
    styleUrls: ['./form-data-driven-form-group.component.css']
})
export class FormDataDrivenFormGroupComponent implements OnInit {

    myForm: FormGroup;
    genders = [
        'male',
        'female',
        'other'
    ];

    hobbies: FormArray;

    constructor() {
        this.myForm = new FormGroup({
            'userInfo' : new FormGroup({
                'mobilephone' : new FormControl('084-73-8025', Validators.required),
                'age' : new FormControl('15', Validators.required),
            }),
            'username' : new FormControl('Marosdee', Validators.required),
            'email' : new FormControl('', [Validators.required, Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
            'password' : new FormControl('', Validators.required),
            'gender' : new FormControl('male', Validators.required),
            'hobbies' : new FormArray([
                new FormControl('Play FIFA Game'),
                new FormControl('Learning')
            ])
        });
        this.hobbies = this.myForm.get('hobbies') as FormArray;
    }

    ngOnInit() {
    }

    onSubmit(): void {
        console.log(this.myForm);
    }

    onAddHobby(): void {
        //(<FormArray>this.myForm.controls.hobbies).push(new FormControl());
        console.debug('add hobby');
        this.hobbies.push(new FormControl());
    }

}
