import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-form-data-driven',
    templateUrl: './form-data-driven.component.html',
    styleUrls: ['./form-data-driven.component.css']
})
export class FormDataDrivenComponent implements OnInit {

    user = {
        username: 'Marosdee',
        email: 'danya.m@excelbangkok.com',
        password: '',
        gender: 'female'
    }

    genders = [
        'male',
        'female',
        'other'
    ]

    constructor() { }

    ngOnInit() {
    }

    onSubmit(form: NgForm): void {
        console.log("It's work", form)
        console.log("Is form validation: ", form.valid)
        console.log('User',this.user)
        console.log('Value', form.value)
    }
}
