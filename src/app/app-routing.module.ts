import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FormComponent } from './form/form.component';
import { LifecycleComponent } from './lifecycle.component';
import { PipesComponent } from './pipes/pipes.component';

const routes: Routes = [
    {
        path: 'home',
        component: FormComponent
    },
    {
        path: 'life',
        component: LifecycleComponent
    },
    {
        path: 'pipe',
        component: PipesComponent
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class FirstProjectRoutingModule { }
