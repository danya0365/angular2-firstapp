import {
    Component,
    OnInit,
    OnChanges,
    DoCheck,
    AfterContentInit,
    AfterContentChecked,
    AfterViewInit,
    AfterViewChecked,
    OnDestroy,
    Input,
    ViewChild,
    ContentChild
} from '@angular/core';

@Component({
  selector: 'app-lifecycle',
  template: `
    <ng-content></ng-content>
    <hr />
    <p #boundParagraph>{{ bindable }}</p>
    <p>{{boundParagraph.textContent}}</p>
  `,
  styles: []
})
export class LifecycleComponent implements OnInit,
OnChanges,
DoCheck,
AfterContentInit,
AfterContentChecked,
AfterViewInit,
AfterViewChecked,
OnDestroy {

    @Input() bindable = 1000;
    @ViewChild('boundParagraph') boundParagraph: HTMLElement;
    @ContentChild('boundContent') boundContent: HTMLElement;

  constructor() { }

  ngOnInit() {
      this.log('ngOnInit');
  }

  ngOnChanges() {
      this.log('ngOnChanges');
  }

  ngDoCheck() {
      this.log('ngDoCheck');
  }

  ngAfterContentInit(){
      this.log('ngAfterContentInit');
      console.info('this.boundContent', this.boundContent);
  }

  ngAfterContentChecked() {
      this.log('ngAfterContentChecked');
  }

  ngAfterViewInit() {
      this.log('ngAfterViewInit');
      console.info('this.boundParagraph', this.boundParagraph);
  }

  ngAfterViewChecked() {
      this.log('ngAfterViewChecked');
  }

  ngOnDestroy() {
      this.log('ngOnDestroy');
  }

  private log(hook: string) {
      var d = new Date();
      var m = d.getMilliseconds();
      var date = d.toString() + ' ' + m;
      console.info(hook + ' ' + date);
  }
}
