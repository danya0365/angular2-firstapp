import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  template: `
  <nav class="navbar navbar-default">
   <div class="container-fluid">
     <div class="navbar-header">
       <a class="navbar-brand"  routerLink="/" routerLinkActive="active">Redirect</a>
     </div>
     <ul class="nav navbar-nav">
       <li routerLinkActive="active"><a routerLink="/home">Home</a></li>
       <li routerLinkActive="active"><a routerLink="/life">life</a></li>
       <li routerLinkActive="active"><a routerLink="/pipe">Pipe</a></li>
     </ul>
   </div>
 </nav>
  <div class="container">
    <router-outlet></router-outlet>
  </div>

  `,
  //styleUrls: ['./app.component.css']
  styles: [`
      h1 {
          color: red
      }
      `]
})
export class AppComponent {
  title: string = 'app works!';
  delete = false;
  text = 'Starting Value';
  boundValue = 1000
  doChangedText() {
      var d = new Date();
      var m = d.getMilliseconds();
      var date = d.toString() + ' ' + m;
      this.text = date;
  }
}
